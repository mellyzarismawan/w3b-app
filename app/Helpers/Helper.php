<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;

class Helper
{
    public static function getCurrentRoute()
    {
        $currentRoute = Route::getCurrentRoute()->getPath();
        $getParams = Route::getCurrentRoute()->parameters();
        if (!empty($getParams)) {
            if (!empty($getParams['id'])) {
                $user_tmp = "{id}";
                $user_id = $getParams['id'];
            } else if (!empty($getParams['user_id'])) {
                $user_tmp = "{user_id}";
                $user_id = $getParams['user_id'];
            }
            $currentRoute = str_replace($user_tmp, $user_id, $currentRoute);
        }
        return $currentRoute;
    }

    public static function splitName($name)
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . $last_name . '#', '', $name));
        return array($first_name, $last_name);
    }
}