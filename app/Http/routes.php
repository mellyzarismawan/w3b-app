<?php

// auth
Route::get('/login', ['uses' => 'AuthController@login', 'as' => 'login']);
Route::post('/login/store', 'AuthController@loginStore')->name('auth.login.store');
Route::get('/register', 'AuthController@register');
Route::post('/register/store', 'AuthController@registerStore')->name('auth.register.store');
Route::get('/logout', 'AuthController@logout');

// main and home
Route::get('/', ['uses' => 'MainController@index', 'as' => 'main']);
Route::get('/home/{id}', ['uses' => 'HomeController@index']);

// profile setting
Route::get('/editprofile', ['uses' => 'ProfileController@edit', 'as' => 'editprofile']);
Route::post('/editprofile/store', 'ProfileController@editStore')->name('profile.edit.store');
Route::post('/changepassword/store', 'ProfileController@changePassword')->name('profile.changepassword.store');
Route::get('/accountsetting', ['uses' => 'ProfileController@accountsetting', 'as' => 'accountsetting']);
Route::post('/accountsetting/store', 'ProfileController@accountsettingStore')->name('profile.accountsetting.store');

// post
Route::get('/my_post/{user_id}', ['uses' => 'PostController@mypost']);
Route::get('/post/{id}', ['uses' => 'PostController@index']);
Route::get('/edit_post/{statuses_id}', ['uses' => 'PostController@editpost']);
Route::get('/delete_post/{statuses_id}', ['uses' => 'PostController@deletepost']);
Route::post('/edit_post/store', 'PostController@editStore')->name('post.edit.store');

// status
Route::get('/status/like', 'StatusController@likeStore')->name('status.like.store');
Route::post('/status/store', 'StatusController@postStore')->name('status.post.store');
Route::post('/status/comment', 'StatusController@commentStore')->name('status.comment.store');

// notification
Route::get('/notification', 'NotificationController@index');

// not found page
Route::get('/not-found', function () {
    return view('errors.404');
});