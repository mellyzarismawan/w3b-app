<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Status;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;
use File;

class ProfileController extends BaseController
{
    public $path;

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $this->user_login = $_SESSION;
        $this->general_controller = new GeneralController();
    }

    public function index()
    {
        if (empty($this->user_login)) {
            return view('errors.404');
        }
        // select users from session .
        $users = User::where('id', $this->user_login['id'])->first();

        $session = $this->user_login;
        if (empty($users)) {
            session_unset();
            session_destroy();
            return redirect()->route('main');
        }
        $session['name'] = $users->getName();
        return view('page.notification', compact('session'));
    }

    public function edit()
    {
        if (empty($this->user_login)) {
            return redirect()->route('main');
        }

        // select users from session .
        $users = User::where('id', $this->user_login['id'])->first();
        if (empty($users)) {
            session_unset();
            session_destroy();
            return redirect()->route('main');
        }
        $users->name = $users->getName();

        $session = $this->user_login;
        // get most popular post
        $popular_post = $this->general_controller->getMostPopularPost();
        // get count post
        $count_post = $this->general_controller->countPost($session['id']);
        // count likeable and visitable from user login
        $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);

        return view('page.editprofile', compact('session', 'users', 'like_visitable', 'count_post', 'popular_post'));
    }


    public function editStore(Request $request)
    {
        // image_upload .
        if (!empty($request->file("image"))) {
            $imageName = str_random(12) . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        }
        // select users from session .
        $users = User::where('id', $this->user_login['id'])->first();
        $tmp_name = explode(" ", $request->input("name"));
        User::firstOrCreate(['id' => $request->input('id')])
            ->update([
                'first_name' => isset($tmp_name[0]) ? $tmp_name[0] : "",
                'last_name' => isset($tmp_name[1]) ? $tmp_name[1] : "",
                'username' => $request->input('username'),
                'image' => isset($imageName) ? $imageName : $users->image,
                'email' => $request->input('email'),
                'mobile' => $request->input('mobile'),
                'gender' => $request->input('gender'),
                'birth' => $request->input('birth')
            ]);

        $this->user_login["id"] = !empty($request->input('id')) ? $request->input('id') : $users->id;
        $this->user_login["username"] = !empty($request->input('username')) ? $request->input('username') : $users->username;
        $this->user_login["email"] = !empty($request->input('email')) ? $request->input('email') : $users->email;
        $this->user_login["mobile"] = !empty($request->input('mobile')) ? $request->input('mobile') : $users->mobile;
        $this->user_login["image"] = isset($imageName) ? $imageName : $users->image;
        $this->user_login["first_name"] = !empty($request->input('first_name')) ? $request->input('first_name') : $users->first_name;
        $this->user_login["last_name"] = !empty($request->input('last_name')) ? $request->input('last_name') : $users->last_name;
        $this->user_login["birth"] = !empty($request->input('birth')) ? $request->input('birth') : $users->birth;
        $this->user_login["gender"] = !empty($request->input('gender')) ? $request->input('gender') : $users->gender;

        $request->session()->flash('success_editprofile', trans('Your account has been updated.'));
        $request->session()->flash('success', trans('Your account has been updated.'));
        return redirect()->route('editprofile');
    }

    // change password
    public function changePassword(Request $request)
    {
        $findByUsername = User::where('id', $request->input("id"))
            ->where('password', md5($request->input("current_password")))
            ->first();
        if ($request->input('new_password') != $request->input('confirm_password')) {
            $request->session()->flash('danger_changepassword', trans('Confirm password does not match'));
            $request->session()->flash('danger', trans('Confirm password does not match'));
            return redirect()->back();
        } else if ($findByUsername['password'] != md5($request->input('current_password'))) {
            $request->session()->flash('danger_changepassword', trans('Incoret password'));
            $request->session()->flash('danger', trans('Incoret password'));
            return redirect()->back();
        } else {
            User::firstOrCreate(['id' => $request->input('id')])
                ->update([
                    'password' => md5($request->input('new_password'))
                ]);
            $request->session()->flash('success_changepassword', trans('Success updated your password'));
            $request->session()->flash('success', trans('Success updated your password'));
            return redirect()->route('editprofile');
        }
    }

    // account setting
    public function accountsetting()
    {
        if (empty($this->user_login)) {
            return redirect()->route('main');
        }
        // select users from session
        $users = User::where('id', $this->user_login['id'])->first();
        if (empty($users)) {
            session_unset();
            session_destroy();
            return redirect()->route('main');
        }
        $users->name = $users->getName();

        $session = $this->user_login;

        // get most popular post
        $popular_post = $this->general_controller->getMostPopularPost();
        // get count post
        $count_post = $this->general_controller->countPost($session['id']);
        // count likeable and visitable from user login
        $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);

        return view('page.accountsetting', compact('session', 'users', 'like_visitable', 'count_post', 'popular_post'));
    }

    // account setting
    public function accountsettingStore(Request $request)
    {
        $is_anonim = $request->input('is_anonim');
        if (isset($is_anonim)) {
            if ($request->input('is_anonim') == 'on') {
                User::firstOrCreate(['id' => $request->input('id')])
                    ->update([
                        'is_anonim' => User::IS_ANONIM
                    ]);
                $this->user_login["is_anonim"] = User::IS_ANONIM;
                $request->session()->flash('success_accountsetting', trans('Success updated your identity to anonim'));
                $request->session()->flash('success', trans('Success updated your identity to anonim'));
                return redirect()->route('editprofile');
            }
        } else {
            User::firstOrCreate(['id' => $request->input('id')])
                ->update([
                    'is_anonim' => User::NOT_ANONIM
                ]);
            $this->user_login["is_anonim"] = User::NOT_ANONIM;
            $request->session()->flash('success_accountsetting', trans('Success updated your identity'));
            $request->session()->flash('success', trans('Success updated your identity'));
            return redirect()->route('editprofile');
        }
    }

}
