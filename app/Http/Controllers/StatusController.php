<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Auth;
use App\Models\Status;
use App\Models\Likeable;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class StatusController extends Controller
{
    protected $username = 'username';

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function postStore(Request $request)
    {
        Status::create([
            'user_id' => $_SESSION['id'],
            'parent_id' => 0,
            'body' => $request->input('body'),
            'status' => 1,
        ]);

        return redirect()->back();
    }

    public function commentStore(Request $request)
    {
        Status::create([
            'user_id' => $_SESSION['id'],
            'parent_id' => $request->input('parent_id'),
            'body' => $request->input('body'),
            'status' => 1,
        ]);

        return redirect()->back();
    }

    public function likeStore(Request $request)
    {
        Likeable::updateOrCreate(
            [
                'user_id' => $_SESSION['id'],
                'statuses_id' => $request->input('statuses_id'),
            ],
            [
                'type' => $request->input('type')
            ]
        );

        return redirect()->back();
    }

}
