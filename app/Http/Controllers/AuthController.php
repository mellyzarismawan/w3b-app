<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $username = 'username';

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function login()
    {
        return view('page.login');
    }

    public function loginStore(Request $request)
    {
        $findByUsername = User::whereRaw("(username = '" . $request->input("username") . "' or email = '" . $request->input("username") . "')")
            ->whereRaw("(password = '" . md5($request->input("password")) . "' or passcode = '" . $request->input("password") . "')")
            ->first();

        if (empty($findByUsername)) {
            $request->session()->flash('danger', trans('Invalid Username Or Password'));
            return redirect()->back();
        } else {
            $_SESSION["id"] = $findByUsername->id;
            $_SESSION["username"] = $findByUsername->username;
            $_SESSION["email"] = $findByUsername->email;
            $_SESSION["mobile"] = $findByUsername->mobile;
            $_SESSION["first_name"] = $findByUsername->first_name;
            $_SESSION["last_name"] = $findByUsername->last_name;
            $_SESSION["image"] = $findByUsername->image;
            $_SESSION["birth"] = $findByUsername->birth;
            $_SESSION["gender"] = $findByUsername->gender;
            $_SESSION["is_anonim"] = $findByUsername->is_anonim;
            $_SESSION["status"] = $findByUsername->status;
            $_SESSION["created_at"] = $findByUsername->created_at;
            $_SESSION["updated_at"] = $findByUsername->updated_at;
            return redirect()->route('main');
        }
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        return redirect()->route('main');
    }

    public function register()
    {
        return view('page.register');
    }

    public function registerStore(Request $request)
    {
        if (empty($request->input("term"))) {
            $request->session()->flash('danger', trans('Please Accept Terms & Conditions.'));
            return redirect()->back();
        } else {
            $findByUsername = User::where('username', $request->input("username"))
                ->first();

            if (isset($findByUsername)) {
                $request->session()->flash('danger', trans('Username Already Been Taken.'));
                return redirect()->back();
            }

            $findByEmail = User::where('email', $request->input("email"))
                ->first();

            if (isset($findByEmail)) {
                $request->session()->flash('danger', trans('Email Already Been Taken.'));
                return redirect()->back();
            }


            $tmp_name = Helper::splitName($request->input("name"));
            User::create([
                'first_name' => isset($tmp_name[0]) ? $tmp_name[0] : "",
                'last_name' => isset($tmp_name[1]) ? $tmp_name[1] : "",
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'password' => md5($request->input('password')),
                'mobile' => $request->input('mobile'),
                'gender' => $request->input('gender'),
                'status' => User::STATUS_ACTIVE,
                'is_anonim' => User::NOT_ANONIM
            ]);

            $request->session()->flash('success', trans('Your account has been created and you can now sign in.'));
            return redirect()->route('login');
        }
    }
}
