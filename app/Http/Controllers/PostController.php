<?php

namespace App\Http\Controllers;

use App\Models\Likeable;
use App\Models\Status;
use App\Models\Visitable;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $this->general_controller = new GeneralController();
    }

    public function index($statuses_id)
    {
        $statuses_id = base64_decode($statuses_id);
        $session = $_SESSION;
        $user_id = isset($session['id']) ? $session['id'] : null;
        $list_post = Status::select('statuses.*', 'users.image')
            ->where("statuses.id", $statuses_id)
            ->join('users', 'users.id', '=', 'user_id')
            ->get();

        $list_visit = Visitable::where("statuses_id", $statuses_id)->where("user_id", $user_id)->first();
        $visitable_count = $list_visit['count'];
        $count_visitable = 1 + $visitable_count;
        Visitable::updateOrCreate(
            [
                'user_id' => $user_id,
                'statuses_id' => $statuses_id,
            ],
            [
                'count' => $count_visitable,
                'type' => Visitable::TYPE_SEE,
            ]
        );

        // get most popular post
        $popular_post = $this->general_controller->getMostPopularPost();

        if (!empty($session)) {
            // select users from session
            $users = User::where('id', $session['id'])->first();
            if (empty($users)) {
                session_unset();
                session_destroy();
                return redirect()->route('main');
            }
            $users->name = $users->getName();
            // count likeable and visitable from user login
            $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);
            // get count post
            $count_post = $this->general_controller->countPost($session['id']);
        }

        return view('page.post', compact('session', 'users', 'list_post', 'popular_post', 'count_post', 'like_visitable'));
    }

    public function mypost($user_id, $page = 1)
    {
        $page = 1;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $user_id = base64_decode($user_id);
        $session = $_SESSION;

        if (empty($session) || $user_id != $session['id']) {
            return redirect()->route('main');
        }

        // count all list post without limit
        $all_list_post = Status::select('statuses.*', 'users.image')
            ->where("parent_id", 0)
            ->where("statuses.user_id", $user_id)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('updated_at', 'desc')->get();

        // count all list post with limit
        $list_post = Status::select('statuses.*', 'users.image')
            ->where("parent_id", 0)
            ->where("statuses.user_id", $user_id)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('updated_at', 'desc')
            ->limit($page * 10);

        if (isset($_GET["status"])) {
            $list_post->where("statuses.status", $_GET["status"]);
        }

        $list_post = $list_post->get();

        $popular_post = $this->general_controller->getMostPopularPost($user_id);

        if (!empty($session)) {
            // select users from session
            $users = User::where('id', $session['id'])->first();
            if (empty($users)) {
                session_unset();
                session_destroy();
                return redirect()->route('main');
            }
            $users->name = $users->getName();

            // count likeable and visitable from user login
            $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);
            // get count post
            $count_post = $this->general_controller->countPost($session['id']);
        }

        return view('page.mypost', compact('session', 'user_id', 'users', 'list_post', 'all_list_post', 'count_post', 'popular_post', 'like_visitable'));
    }

    public function editpost($statuses_id)
    {
        $statuses_id = base64_decode($statuses_id);
        $session = $_SESSION;
        $user_id = isset($session['id']) ? $session['id'] : null;
        $list_post = Status::select('statuses.*', 'users.image')
            ->where("statuses.id", $statuses_id)
            ->join('users', 'users.id', '=', 'user_id')
            ->first();

        $popular_post = $this->general_controller->getMostPopularPost();

        if (!empty($session)) {
            // select users from session
            $users = User::where('id', $session['id'])->first();
            if (empty($users)) {
                session_unset();
                session_destroy();
                return redirect()->route('main');
            }
            $users->name = $users->getName();

            // count likeable and visitable from user login
            $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);
            // get count post
            $count_post = $this->general_controller->countPost($session['id']);
        }

        return view('page.editpost', compact('session', 'user_id', 'users', 'list_post', 'count_post', 'popular_post', 'like_visitable'));
    }


    public function editStore(Request $request)
    {
        Status::firstOrCreate(['id' => $request->input('id')])
            ->update([
                'body' => $request->input('body'),
                'status' => $request->input('status'),
            ]);

        return redirect()->route('main');
    }

    public function deletepost($statuses_id)
    {
        $statuses_id = base64_decode($statuses_id);
        $post = Status::find($statuses_id);
        if (empty($post)) {
            return redirect()->back();
        }
        $post->delete();
        return redirect()->back();
    }
}
