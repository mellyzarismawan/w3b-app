<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Models\User;
use App\Models\Status;
use App\Models\Visitable;

class HomeController extends BaseController
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $this->general_controller = new GeneralController();
    }

    public function index($user_id)
    {
        $user_id = base64_decode($user_id);
        $session = $_SESSION;
        $page = 1;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }

        if (empty($session) || $user_id != $session['id']) {
            return redirect()->route('main');
        }
        $list_post = $this->general_controller->getListPost($user_id, $page);
        $all_list_post = $this->general_controller->getAllListPost($user_id);

        $popular_post = $this->general_controller->getMostPopularPost($user_id);

        if (!empty($session)) {
            // select users from session
            $users = User::where('id', $session['id'])->first();
            if (empty($users)) {
                session_unset();
                session_destroy();
                return redirect()->route('main');
            }
            $users->name = $users->getName();

            // count likeable and visitable from user login
            $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);
            // get count post
            $count_post = $this->general_controller->countPost($session['id']);
        }

        return view('page.home', compact('session', 'user_id', 'users', 'list_post', 'all_list_post', 'count_post', 'popular_post', 'like_visitable'));
    }
}
