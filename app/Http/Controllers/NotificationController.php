<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Routing\Controller as BaseController;

class NotificationController extends BaseController
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $this->user_login = $_SESSION;
    }

    public function index()
    {
        if (empty($this->user_login)) {
            return redirect()->route('main');
        }

        // select users from session..
        $users = User::where('id', $this->user_login['id'])->first();
        if (empty($users)) {
            session_unset();
            session_destroy();
            return redirect()->route('main');
        }
        $users->name = $users->getName();

        $session = $this->user_login;
        return view('page.notification', compact('session', 'users'));
    }
}
