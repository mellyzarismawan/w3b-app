<?php

namespace App\Http\Controllers;

use App\Models\Likeable;
use App\Models\User;
use App\Models\Visitable;
use App\Models\Status;
use Illuminate\Routing\Controller as BaseController;

class GeneralController extends BaseController
{
    public function __construct()
    {
    }

    public function getAllListPost($user_id = "")
    {
        $all_list_post = Status::select('statuses.*', 'users.image')
            ->where("statuses.status", Status::STATUS_ACTIVE)
            ->where("parent_id", 0)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('updated_at', 'desc')
            ->orderBy('id', 'desc');

        if (!empty($user_id)) {
            $all_list_post->where("statuses.user_id", $user_id);
        }

        return $all_list_post->get();
    }

    public function getListPost($user_id = "", $page = 1)
    {
        $list_post = Status::select('statuses.*', 'users.image')
            ->where("statuses.status", Status::STATUS_ACTIVE)
            ->where("parent_id", 0)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('updated_at', 'desc')
            ->orderBy('id', 'desc')
            ->limit($page*10);

        if (!empty($user_id)) {
            $list_post->where("statuses.user_id", $user_id);
        }

        return $list_post->get();
    }

    /**
     * this function for count likeable and visitable from user login
     * @param int $user_id
     * @return array
     */
    public function countLikeableAndVisitable($user_id)
    {
        // define current date and date last week for parameter get data last week
        $currentdate = date('Y-m-d H:i:s');
        $date_last_week = date('Y-m-d H:i:s', strtotime("-7 day", strtotime($currentdate)));
        // count all likes from user login
        $like = Likeable::where('type', Likeable::LIKEABLE)
            ->where('statuses.user_id', $user_id)
            ->where('statuses.deleted_at','=', null)
            ->join('statuses', 'statuses.id', '=', 'statuses_id')
            ->get()
            ->count();
        // count like this week from user login
        $like_this_week = Likeable::where('type', Likeable::LIKEABLE)
            ->where('statuses.user_id', $user_id)
            ->where('statuses.deleted_at','=', null)
            ->whereBetween('likeable.created_at', array($date_last_week, $currentdate))
            ->join('statuses', 'statuses.id', '=', 'statuses_id')
            ->get()
            ->count();
        // count all visitable from user login
        $visit_query = Visitable::where('type', Visitable::TYPE_SEE)
            ->where('statuses.user_id', $user_id)
            ->where('statuses.deleted_at','=', null)
            ->join('statuses', 'statuses.id', '=', 'statuses_id')
            ->get();
        $visitable = $visit_query->sum('count');
        // count visitable this week from user login
        $visit_this_week_query = Visitable::where('type', Visitable::TYPE_SEE)
            ->where('statuses.user_id', $user_id)
            ->where('statuses.deleted_at','=', null)
            ->whereBetween('visitable.created_at', array($date_last_week, $currentdate))
            ->join('statuses', 'statuses.id', '=', 'statuses_id')
            ->get();
        $visitable_this_week = $visit_this_week_query->sum('count');
        return [
            'like' => $like,
            'like_this_week' => $like_this_week,
            'visitable' => $visitable,
            'visitable_this_week' => $visitable_this_week
        ];
    }

    /**
     * this function for count post from user login
     * @param int $user_id
     * @return array
     */
    public function countPost($user_id)
    {
        // count all status
        $all = Status::where("parent_id", 0)->where("user_id", $user_id)->count();
        // count status with status still draft
        $draft = Status::where("parent_id", 0)->where("user_id", $user_id)->where("status", 2)->count();
        // count status with status published
        $publish = Status::where("parent_id", 0)->where("user_id", $user_id)->where("status", 1)->count();
        return [
            'all' => $all,
            'draft' => $draft,
            'publish' => $publish
        ];
    }

    /**
     * this function for get most popular
     * @param int $user_id
     * @return array
     */
    public function getMostPopularPost($user_id = "")
    {
        $popular_post = Status::selectRaw("statuses.id,statuses.user_id, statuses.body, count(likeable.id) as likes")
            ->join("likeable", "likeable.statuses_id", "=", "statuses.id")
            ->where("statuses.status", Status::STATUS_ACTIVE)
            ->where("statuses.parent_id", 0)
            ->groupBy("statuses.id")
            ->orderBy("likes", "desc")
            ->limit(5);

        if (!empty($user_id)) {
            $popular_post->where("statuses.user_id", $user_id);
        }

        return $popular_post->get();
    }
}
