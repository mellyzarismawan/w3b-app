<?php

namespace App\Http\Controllers;

use App\Models\Likeable;
use App\Models\Status;
use App\Models\User;
use App\Models\Visitable;
use Illuminate\Routing\Controller as BaseController;

class MainController extends BaseController
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $this->general_controller = new GeneralController();
    }

    public function index()
    {
        $session = $_SESSION;
        $page = 1;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        // get list post
        $list_post = $this->general_controller->getListPost("", $page);
        // get all list post user
        $all_list_post = $this->general_controller->getAllListPost("");
        // get most popular post
        $popular_post = $this->general_controller->getMostPopularPost();

        if (!empty($session)) {
            // select users from session
            $users = User::where('id', $session['id'])->first();
            if (empty($users)) {
                session_unset();
                session_destroy();
                return redirect()->route('main');
            }
            $users->name = $users->getName();
            // count likeable and visitable from user login
            $like_visitable = $this->general_controller->countLikeableAndVisitable($session['id']);
            // get count post
            $count_post = $this->general_controller->countPost($session['id']);
        }

        return view('page.main', compact('session', 'users', 'list_post', 'all_list_post', 'like_visitable', 'popular_post', 'count_post'));
    }
}
