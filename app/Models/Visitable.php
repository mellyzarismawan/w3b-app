<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use App\Models\User;
use App\Models\Status;


class Visitable extends Model implements AuthenticatableContract,
    AuthorizableContract

{
    use Authenticatable, Authorizable;

    CONST TYPE_SEE = 1;

    protected $table = 'visitable';

    protected $fillable = [
        'user_id',
        'statuses_id',
        'type',
        'count'
    ];

    protected $hidden = [
    ];

}
