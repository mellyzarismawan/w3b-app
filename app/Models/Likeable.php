<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use App\Models\User;
use App\Models\Status;


class Likeable extends Model implements AuthenticatableContract,
    AuthorizableContract

{
    use Authenticatable, Authorizable;

    CONST STATUS_ACTIVE = 1;
    CONST LIKEABLE = 1;
    CONST DISLIKE = 2;

    protected $table = 'likeable';

    protected $fillable = [
        'user_id',
        'statuses_id',
        'type',
    ];

    protected $hidden = [
    ];

}
