<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Str;

class Status extends Model implements AuthenticatableContract,
    AuthorizableContract

{
    use SoftDeletes;
    use Authenticatable, Authorizable;

    CONST STATUS_ACTIVE = 1;

    protected $table = 'statuses';

    protected $fillable = [
        'user_id',
        'parent_id',
        'body',
        'status',
    ];

    protected $appends = [
        'publish_at',
        'count_like'
    ];

    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo(Status::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Status::class, 'parent_id');
    }

    public function likeable()
    {
//        return $this->belongsTo(Likeable::class, 'id', 'statuses_id');
        return $this->belongsTo(Likeable::class, 'id', 'statuses_id');
    }

    public function getPublishAtAttribute()
    {
        return date("F, j Y H:i", strtotime($this->attributes['created_at']));
    }

    public function getHasLikeAttribute()
    {
        $likeable = Likeable::selectRaw("type")->where("statuses_id",$this->attributes['id'])->where("user_id",$_SESSION['id'])->where("type",1)->first();
        return (!empty($likeable)) ? $likeable->type : 2 ;
    }

    public function getCountLikeAttribute()
    {
        $likeable = Likeable::selectRaw("count(id) as count_like")->where("statuses_id",$this->attributes['id'])->where("type",1)->first();
        return $likeable->count_like;
    }

    public function getCountViewAttribute()
    {
        $visitable = Visitable::selectRaw("sum(count) as count_view")->where("statuses_id",$this->attributes['id'])->where("type",1)->first();
        return $visitable->count_view;
    }

    public function getBodyLimit20Attribute()
    {
        return Str::words($this->attributes['body'], '20');
    }

    public function getBodyLimit100Attribute()
    {
        return Str::words($this->attributes['body'], '100');
    }

    public function getCommentLimitAttribute()
    {
        $status = Status::select('statuses.*', 'users.image')
            ->where("statuses.parent_id",$this->attributes['id'])
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy("statuses.created_at","asc")
            ->take(3)
            ->get();
        return $status;
    }

    public function getCommentsAttribute()
    {
        $status = Status::select('statuses.*', 'users.image')
            ->where("statuses.parent_id",$this->attributes['id'])
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy("statuses.created_at","asc")
            ->get();
        return $status;
    }
}
