<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Status;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


class User extends Model implements AuthenticatableContract,
    AuthorizableContract

{
    use Authenticatable, Authorizable;


    CONST LOGIN = true;
    CONST NOT_LOGIN = false;
    CONST STATUS_ACTIVE = 1;
    CONST NOT_ANONIM = 2;
    CONST IS_ANONIM = 1;

    protected $table = 'users';


    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'status',
        'is_anonim',
        'gender',
        'mobile',
        'image',
        'birth'
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getName()
    {
        if ($this->first_name && $this->last_name) {
            return "{$this->first_name} {$this->last_name}";
        }

        if ($this->first_name) {
            return $this->first_name;
        }

        return null;
    }

    public function getNameOrUsername()
    {
        return $this->getName() ?: $this->username;
    }

    public function getFirstNameOrUsername()
    {
        return $this->first_name ?: $this->username;
    }


    public function getAnonimNameAttribute()
    {
        return substr($this->first_name,0,3);
    }
}
