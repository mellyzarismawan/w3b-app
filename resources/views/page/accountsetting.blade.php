@if(!empty($session))
<!DOCTYPE html>
<html lang="en">
<head>
    @include('source.headsource')
</head>
<body>
<div class="theme-layout">
    @include('layout.header', ['login'=>$session])
    <section>
        <div class="gap2 gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">
                            <div class="col-lg-9">
                                @include('content.accountsetting', ['login'=>$session, 'users'=>$users])
                            </div><!-- centerl meta -->
                            <div class="col-lg-3">
                                <aside class="sidebar static right">
                                    @if ($session)
                                        @include('content.summarypost')
                                        @include('content.countpost')
                                    @endif
                                    @include('content.mostpopular')
                                </aside>
                            </div><!-- sidebar -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layout.footer')
</div>
@include('source.footsource')
</body>
</html>
@endif