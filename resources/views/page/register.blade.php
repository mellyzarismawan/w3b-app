<!DOCTYPE html>
<html lang="en">
<head>
    @include('source.headsource')
</head>
<style>
    .already-have-account {
        color: #088dcd;
        font-size: 15px;
    }

    .already-have-account:hover {
        color: #088dcd;
        font-size: 15px;
        text-decoration: underline;
    }
</style>
<body>
<div class="theme-layout">
    <div class="container-fluid pdng0">
        <div class="row merged">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="login-reg-bg">
                    <div class="log-reg-area">
                        <h2 class="log-title">Register</h2>
                        <br>
                        @include('_partials.notifications')<br>
                        <form method="post" role="form" action="{{ route('auth.register.store') }}">
                            <div class="form-group">
                                <input type="text" name="name" required="required"/>
                                <label class="control-label" for="input">Name</label><i
                                        class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" required="required"/>
                                <label class="control-label" for="input">Username</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" required="required"/>
                                <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" required="required"/>
                                <label class="control-label" for="input"><a href="/cdn-cgi/l/email-protection"
                                                                            class="__cf_email__"
                                                                            data-cfemail="7b3e161a12173b">Email</a></label><i
                                        class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="text" name="mobile" required="required"/>
                                <label class="control-label" for="input">Mobile</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-radio">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" checked="checked" value="2"/><i class="check-box"></i>Male
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" value="1"/><i class="check-box"></i>Female
                                    </label>
                                </div>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="term"/><i class="check-box"></i>Accept
                                    Terms &
                                    Conditions ?
                                </label>
                            </div>
                            <div>
                                <a href="{{URL::to('/login')}}" title="" class="already-have-account">Already have an
                                    account</a>
                            </div>
                            <div>
                                <button class="mtr-btn" type="submit"><span>Register</span></button>
                            </div>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="land-featurearea">
                    <div class="land-meta">
                        <h1>w3b - app</h1>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        <p>been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                            a</p>
                        galley of type and scrambled it to make a type specimen book.
                        </p>
                        <div class="friend-logo">
                            <span><img src="../resources/assets/images/wink.png" alt=""></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('source.footsource')
</body>
</html>