<!DOCTYPE html>
<html lang="en">
<head>
    @include('source.headsource')
</head>
<body>
<div class="theme-layout">
    <div class="container-fluid pdng0">
        <div class="row merged">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="login-reg-bg">
                    <div class="log-reg-area sign">
                        <h2 class="log-title">Login</h2>
                        <br>
                        @include('_partials.notifications')<br>
                        <form method="post" role="form" action="{{ route('auth.login.store') }}">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <input type="text" name="username" id="username" required="required"/>
                                <label class="control-label" for="username">Username / Email</label><i class="mtrl-select"></i>
                                @if($errors->has('username'))
                                    <span class="help-block">{{ $errors->first('username') }}</span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" name="password" id="password" required="required"/>
                                <label class="control-label" for="password">Password</label><i class="mtrl-select"></i>
                                @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <div>
                                <button class="mtr-btn" type="submit"><span>Login</span></button>
                                <a href="{{URL::to('/register')}}">
                                    <button class="mtr-btn" type="button"><span>Register</span></button>
                                </a>
                            </div>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="land-featurearea">
                    <div class="land-meta">
                        <h1>w3b - app</h1>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        <p>been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                            a</p>
                        galley of type and scrambled it to make a type specimen book.
                        </p>
                        <div class="friend-logo">
                            <span><img src="../resources/assets/images/wink.png" alt=""></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('source.footsource')
</body>
</html>