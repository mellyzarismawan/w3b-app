<div class="postoverlay"></div>
<div class="responsive-header">
    <div class="mh-head first Sticky">
		<span class="mh-btns-left">
			<a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
		</span>
        <span class="mh-text">
			<a href="{{URL::to('/')}}" title=""><img src="{{URL::to("../resources/assets/images/webapp.png")}}" alt=""></a>
		</span>
        <span class="mh-btns-right">

		</span>
    </div>
    <nav id="menu" class="res-menu">
        @if (empty($session))
            <ul>
                <li><a href="{{URL::to('/login')}}" title="">Sign In.</a></li>
                <li><a href="{{URL::to('/register')}}" title="">Sign Up</a></li>
            </ul>
        @endif

        @if (!empty($session))
            <ul>
                <li><a href="{{URL::to('/home/'.base64_encode($_SESSION['id']))}}" title="">Home</a></li>
                <li>
                    <span>Profile</span>
                    <ul>
                        <li><a href="{{URL::to('/editprofile')}}" title="">edit profile</a></li>
                        <li><a href="{{URL::to('/logout')}}" title="">log out</a></li>
                    </ul>
                </li>
            </ul>
        @endif
    </nav>

</div><!-- responsive header -->

<div class="topbar stick">
    <div class="logo">
        <a title="" href="{{URL::to('/')}}"><img src="{{URL::to("../resources/assets/images/webapp.png")}}" alt="" width="100"></a>
    </div>

    <div class="top-area">
        @if (empty($session))
            <div class="login-form" style="padding-right: 10px; padding-top: 10px">
                <a href="{{URL::to('/login')}}" class="btn btn-primary">Sign In</a>
                <a href="{{URL::to('/register')}}" class="btn btn-primary">Sign Up</a>
            </div>
        @endif

        @if (!empty($session))
            <ul>
                <li>
                    <a href="{{URL::to('/home/'.base64_encode($_SESSION['id']))}}" title="Home" data-ripple=""><i class="ti-home"></i></a>
                </li>
                <li>
                    <a href="{{URL::to('/')}}" title="Beranda" data-ripple=""><i class="fa fa-globe"></i></a>
                </li>
            </ul>

            <div class="user-img">
                {{ $users->name }}
                @if (!empty($users->image))
                    <img src="{{URL::to("../public/images/".$users->image)}}" alt="" style="width:60px; height:60px;">
                @else
                    <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}" alt=""
                         style="width:60px; height:60px;">
                @endif
                <div class="user-setting">
                    <a href="{{URL::to('/editprofile')}}" title=""><i class="ti-pencil-alt"></i>edit profile</a>
                    <a href="{{URL::to('/logout')}}" title=""><i class="ti-power-off"></i>log out</a>
                </div>
            </div>
        @endif
    </div>
</div><!-- topbar -->