<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
    }
</style>
<div class="bottombar footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">© W3b App 2019. All rights reserved.</span>
            </div>
        </div>
    </div>
</div>