<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>W3b App</title>
<link rel="icon" href="{{URL::to("../resources/assets/images/webapp-tumb.png") }}" type="image/png" sizes="16x16">

<link rel="stylesheet" href="{{ asset('../resources/assets/css/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('../resources/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('../resources/assets/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('../resources/assets/css/color.css') }}">
<link rel="stylesheet" href="{{ asset('../resources/assets/css/responsive.css') }}">
{{--<link rel="stylesheet" href="{{ asset('../resources/assets/css/dark-theme.css') }}">--}}