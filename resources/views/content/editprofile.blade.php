<link rel="stylesheet" href="{{URL::to("../resources/assets/jquery-ui/jquery-ui.min.css")}}">
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<script src="{{URL::to("../resources/assets/js/jquery.js")}}"></script>
<script src="{{URL::to("../resources/assets/jquery-ui/jquery-ui.min.js")}}"></script>

<div class="central-meta">
    <div class="editing-info">
        <h5 class="f-title"><i class="ti-info-alt"></i>Edit Profile Info.</h5>
        @if(Session::get('danger_editprofile'))
            @include('_partials.notifications')
        @endif

        @if(Session::get('success_editprofile'))
            @include('_partials.notifications')
        @endif
        <form method="post" role="form" action="{{ route('profile.edit.store') }}" enctype="multipart/form-data">
            <div class="form-group">
                <input type="text" id="input" required="required" name="name" value="{{ $users->name }}"/>
                <label class="control-label" for="input" name="name">Name</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="text" required="required" name="username" value="{{ $users->username }}"/>
                <label class="control-label" for="input" name="username">Username</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="text" required="required" name="email" value="{{ $users->email }}"/>
                <label class="control-label" for="input"><a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                            data-cfemail="abeec6cac2c7eb">Email</a></label><i
                        class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="text" required="required" name="mobile" value="{{ $users->mobile }}"/>
                <label class="control-label" for="input">Mobile</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="text" required="required" name="birth" id="datepicker"
                       value="{{ $users->birth }}"/>
                <label class="control-label" for="input">Tanggal Lahir</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <label style="color: #078ccd; font-size: 14px; margin-left: 4px;">Image</label>
                <input type="file" name="image" value="{{ $users->image }}"/>
            </div>
            <div class="form-radio">
                <div class="radio">
                    <label>
                        <input type="radio" name="gender"
                               value="2" <?= $users->gender == 2 ? 'checked' : '' ?>><i
                                class="check-box"></i>Male
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="gender"
                               value="1" <?= $users->gender == 1 ? 'checked' : '' ?>><i
                                class="check-box"></i>Female
                    </label>
                </div>
            </div>
            <div class="submit-btns">
                <button type="submit" class="mtr-btn"><span>Save</span></button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <input type="hidden" name="id" value="{{ $users->id }}">
        </form>
    </div>
</div>

<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
    });
</script>