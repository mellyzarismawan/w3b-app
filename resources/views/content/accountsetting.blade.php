<div class="central-meta">
    <div class="onoff-options">
        <h5 class="f-title"><i class="ti-settings"></i>account setting</h5>
        @if(Session::get('success_accountsetting'))
            @include('_partials.notifications')
        @endif
        <form method="post" role="form" action="{{ route('profile.accountsetting.store') }}">
            <div class="setting-row">
                <span>Enable Anonymouss</span>
                <p>You'll become anonymous.</p>
                <input type="checkbox" name="is_anonim" {{ ($users->is_anonim == 1) ? "checked" : "" }} id="switch"/>
                <label for="switch" data-on-label="ON" data-off-label="OFF"></label>
            </div>
            <div class="submit-btns">
                <button type="submit" class="mtr-btn"><span>Save</span></button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <input type="hidden" name="id" value="{{ $users->id }}">
        </form>
    </div>
</div>