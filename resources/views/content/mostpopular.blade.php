<div class="widget stick-widget">
    <h4 class="widget-title">Most Popular Post</h4>
    <ul class="short-profile">
        @if(count($popular_post) > 0)
            @foreach($popular_post as $post)
                <li class="pointing">
                    <span>{{$post->user->first_name}} {{$post->user->last_name}}</span>
                    <p>
                        {!! nl2br(htmlspecialchars_decode(stripslashes(str_limit(strip_tags($post->body), 50)))) !!}
                        <a href="{{URL::to('/post/'.base64_encode($post->id))}}"><span
                                    style="color: #1b78c7">Read More</span></a>
                    </p>
                </li>
            @endforeach
        @else
            Data Not Found
        @endif
    </ul>
</div>