<div class="widget">
    <h4 class="widget-title">Your page</h4>
    <div class="your-page">
        <div class="page-likes">
            <ul class="nav nav-tabs likes-btn">
                <li class="nav-item"><a class="active" href="#link1" data-toggle="tab">likes</a></li>
                <li class="nav-item"><a class="" href="#link2" data-toggle="tab">views</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active fade show " id="link1">
                    <span><i class="ti-heart"></i>{{ $like_visitable['like']  }}</span>
                    <a href="#" title="weekly-likes">{{ $like_visitable['like_this_week'] }} new likes this week</a>
                </div>
                <div class="tab-pane fade" id="link2">
                    <span><i class="ti-eye"></i>{{ $like_visitable['visitable']  }}</span>
                    <a href="#" title="weekly-likes">{{ $like_visitable['visitable_this_week'] }} new views this
                        week</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- page like widget -->