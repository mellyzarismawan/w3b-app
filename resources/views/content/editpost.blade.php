<div class="central-meta">
    <div class="editing-info">
        <h5 class="f-title"><i class="ti-info-alt"></i>Edit Post</h5>
        <form method="post" role="form" action="{{ route('post.edit.store') }}" enctype="multipart/form-data">

            <textarea style="border-bottom: 1px solid #eeeeee !important" name="body" rows="5" placeholder="write something">{{ $post->body }}</textarea>
            <div class="form-radio">
                <div class="radio">
                    <label>
                        <input type="radio" name="status"
                               value="1" {{ $post->status == 1 ? 'checked' : '' }}><i
                                class="check-box"></i>Publish
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status"
                               value="2" {{ $post->status == 2 ? 'checked' : '' }}><i
                                class="check-box"></i>Unpublish
                    </label>
                </div>
            </div>
            <div class="submit-btns">
                <button type="submit" class="mtr-btn"><span>Save</span></button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <input type="hidden" name="id" value="{{ $post->id }}">
        </form>
    </div>
</div>