<style>
    .action-post {
        text-align: right;
    }

    .edit-post {
        text-align: right;
        color: #8c8b8b;
        cursor: pointer;
    }

    .edit-post:hover {
        color: #1b78c7;
    }

    .delete-post {
        text-align: right;
        color: #8c8b8b;
        cursor: pointer;
    }

    .delete-post:hover {
        color: red;
    }

    .post-publish {
        float: left;
        color: forestgreen;
        border: 1px solid;
        padding: 2px 5px;
        border-radius: 10px;
    }

    .post-unpublish {
        float: left;
        color: darkred;
        border: 1px solid;
        padding: 2px 5px;
        border-radius: 10px;
    }
</style>
@if(count($list_post) > 0)
    @foreach($list_post as $post)
        <div class="central-meta item">
            <div class="user-post">
                <div class="action-post">
                    @if($post->status == 1)
                        <span class="post-publish">Publish</span>
                    @else
                        <span class="post-unpublish">Unpublish</span>
                    @endif
                    <a href="{{URL::to('/edit_post/'.base64_encode($post->id))}}"><i title="Edit"
                                                                                     class="fa fa-pencil edit-post"></i></a>
                    <a href="{{URL::to('/delete_post/'.base64_encode($post->id))}}"
                       onclick="return confirm('Are you sure you want to delete this item?');"><i title="Delete"
                                                                                                  class="fa fa-trash delete-post"></i></a>
                </div>
                <div class="friend-info">
                    <figure>
                        @if (!empty($post->image))
                            <img src="{{URL::to("../public/images/".$post->image)}}" alt=""
                                 style="width:60px; height:60px;">
                        @else
                            <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}"
                                 alt=""
                                 style="width:60px; height:60px;">
                        @endif
                    </figure>
                    <div class="friend-name">
                        <ins><a title="">{{$post->user->first_name}} {{$post->user->last_name}}</a></ins>
                        <span>created at : {{$post->publish_at}}</span>
                    </div>
                    <div class="post-meta">
                        <div class="description">
                            <p>
                                {!! nl2br(htmlspecialchars_decode(stripslashes($post->body_limit_100))) !!}
                                <br>
                                @if (strlen($post->body) > 300)
                                    <a href="{{URL::to('/edit_post/'.base64_encode($post->id))}}" style="color:#088dcd">Read More</a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    @include('_partials.norecord')
@endif
<?php
use App\Helpers\Helper;
$page = 2;
if (isset($_GET['page'])) {
    $page = $_GET['page'] + 1;
}
$currentRoute = \App\Helpers\Helper::getCurrentRoute();
?>
@if(count($all_list_post)>10)
    <a href="{{URL::to($currentRoute."?page=".$page)}}">
        <button class="btn-view btn-load-more"></button>
    </a>
@endif