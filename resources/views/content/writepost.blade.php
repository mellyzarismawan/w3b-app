@if (!empty($session))
<div class="central-meta new-pst">
    <div class="new-postbox">
        <figure>
            @if (!empty($users->image))
                <img src="{{URL::to("../public/images/".$users->image)}}" alt="" style="width:60px; height:60px;">
            @else
                <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}" alt=""
                     style="width:60px; height:60px;">
            @endif
        </figure>
        <div class="newpst-input">
            <form method="post" action="{{ route('status.post.store') }}">
                <textarea name="body" rows="2" placeholder="write something"></textarea>
                <div class="attachments">
                    <ul>
                        <li>
                            <button type="submit">Post</button>
                        </li>
                    </ul>
                </div>
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
        </div>
    </div>
</div><!-- add post new box -->
@endif