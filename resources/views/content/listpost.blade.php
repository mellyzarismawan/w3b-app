<style>
    .load-comment {
        color: #8c8b8b;
        width: 100%;
        font-size: 13px;
        cursor: pointer;
        text-align: center;
    }

    .load-comment:hover {
        color: #1b78c7;
    }
</style>
@if(count($list_post) > 0)
    @foreach($list_post as $post)
        <div class="central-meta item">
            <div class="user-post">
                <div class="friend-info">
                    <figure>
                        @if (!empty($post->image))
                            <img src="{{URL::to("../public/images/".$post->image)}}" alt=""
                                 style="width:60px; height:60px;">
                        @else
                            <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}" alt=""
                                 style="width:60px; height:60px;">
                        @endif
                    </figure>
                    <div class="friend-name">
                        @if ($post->user->is_anonim == 1)
                            <ins><a title="">{{$post->user->anonim_name}}*****</a></ins>
                        @else
                            <ins><a title="">{{$post->user->first_name}} {{$post->user->last_name}}</a></ins>
                        @endif
                        <span>published: {{$post->publish_at}}</span>
                    </div>
                    <div class="post-meta">

                        <div class="description">
                            <p>
                                {!! nl2br(htmlspecialchars_decode(stripslashes($post->body_limit_20))) !!}
                                <br>
                                <a href="{{URL::to('/post/'.base64_encode($post->id))}}" style="color:#088dcd">Read
                                    More</a>
                            </p>
                        </div>
                        <div class="we-video-info">
                            <ul>
                                <li>
                                <span class="views" data-toggle="tooltip" title="views">
                                    <i class="fa fa-eye"></i>
                                    <ins>{{$post->count_view}}</ins>
                                </span>
                                </li>
                                <li>
                                <span class="comment" data-toggle="tooltip" title="Comments">
                                    <a href="{{URL::to('/post/'.base64_encode($post->id))}}"><i
                                                class="fa fa-comments-o"></i></a>
                                    <ins>{{ count($post->children()->get()) }}</ins>
                                </span>
                                </li>

                                <li>
                                <span class="like" data-toggle="tooltip" title="like">
                                    @if (!empty($session))
                                        @if (!empty($post->likeable) && $post->has_like == 1)
                                            <a href="{{ route('status.like.store', ['statuses_id' => $post->id, 'type' => 2]) }}"><i
                                                        class="fa fa-heart" style="color: red"></i></a>
                                        @else
                                            <a href="{{ route('status.like.store', ['statuses_id' => $post->id, 'type' => 1]) }}"><i
                                                        class="ti-heart" style="color: red"></i></a>
                                        @endif
                                    @else
                                        <a><i class="fa fa-heart" style="color: red"></i></a>
                                    @endif
                                    <ins style="color: black">{{$post->count_like}}</ins>
                                </span>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="coment-area">
                    <ul class="we-comet">
                        @foreach($post->comment_limit as $comment)
                            <li>
                                <div class="comet-avatar">
                                    @if (!empty($comment->image))
                                        <img src="{{URL::to("../public/images/".$comment->image)}}" alt="image"
                                             style="width:60px; height:40px;">
                                    @else
                                        <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}"
                                             alt="image" style="width:60px; height:40px;">
                                    @endif
                                </div>
                                <div class="we-comment">
                                    <div class="coment-head">
                                        @if ($comment->user->is_anonim == 1)
                                            <h5><a title="">{{$comment->user->anonim_name}}
                                                    *****</a>
                                            </h5>
                                        @else
                                            <h5><a title="">{{$comment->user->first_name}} {{$comment->user->last_name}}</a>
                                            </h5>
                                        @endif
                                        <span>{{$comment->publish_at}}</span>
                                    </div>
                                    <p style="width: 100% !important;">
                                        {!! nl2br(htmlspecialchars_decode(stripslashes($comment->body))) !!}
                                    </p>
                                </div>
                            </li>
                        @endforeach
                        @if(count($post->children()->get()) > 3)
                            <li>
                                <div class="load-comment">
                                    <a href="{{URL::to('/post/'.base64_encode($post->id))}}">Load more comment</a>
                                </div>
                            </li>
                        @endif
                        @if (!empty($session))
                            <li class="post-comment">
                                <div class="">
                                    <form method="post" action="{{ route('status.comment.store') }}">
                                        <input type="hidden" name="parent_id" value="{{$post->id}}">
                                        <textarea name="body" placeholder="Post your comment"
                                                  style="border-bottom: 1px solid #eeeeee"></textarea>

                                        <button type="submit">Post</button>
                                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    </form>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
@else
    @include('_partials.norecord')
@endif
<?php
use App\Helpers\Helper;
$page = 2;
if (isset($_GET['page'])) {
    $page = $_GET['page'] + 1;
}
$currentRoute = \App\Helpers\Helper::getCurrentRoute();
?>
@if(count($all_list_post)>10)
    <a href="{{URL::to($currentRoute."?page=".$page)}}">
        <button class="btn-view btn-load-more"></button>
    </a>
@endif