<div class="central-meta">
    <div class="editing-info">
        <h5 class="f-title"><i class="ti-lock"></i>Change Password.</h5>
        @if(Session::get('danger_changepassword'))
            @include('_partials.notifications')
        @endif
        @if(Session::get('success_changepassword'))
            @include('_partials.notifications')
        @endif
        <form method="post" role="form" action="{{ route('profile.changepassword.store') }}">
            <div class="form-group">
                <input type="password" name="current_password" required="required"/>
                <label class="control-label" for="input">Current password</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="password" id="input" name="new_password" required="required"/>
                <label class="control-label" for="input">New password</label><i class="mtrl-select"></i>
            </div>
            <div class="form-group">
                <input type="password" name="confirm_password" required="required"/>
                <label class="control-label" for="input">Confirm password</label><i class="mtrl-select"></i>
            </div>
            <div class="submit-btns">
                <button type="submit" class="mtr-btn"><span>Save</span></button>
            </div>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <input type="hidden" name="id" value="{{ $users->id }}">
        </form>
    </div>
</div>