<div class="widget">
    <h4 class="widget-title">Post</h4>
    <ul class="short-profile">
        <li>
            <a href="{{URL::to('/my_post/'.base64_encode($session['id']))}}"><span class="pointing">All ({{$count_post['all']}})</span></a>
        </li>
        <li>
            <a href="{{URL::to('/my_post/'.base64_encode($session['id'])."?status=2")}}"><span class="pointing">Unpublish ({{$count_post['draft']}})</span></a>
        </li>
        <li>
            <a href="{{URL::to('/my_post/'.base64_encode($session['id'])."?status=1")}}"><span class="pointing">Publish ({{$count_post['publish']}})</span></a>
        </li>
    </ul>
</div>