@foreach($list_post as $post)
    <div class="central-meta item">
        <div class="user-post">
            <div class="friend-info">
                <figure>
                    @if (!empty($post->image))
                        <img src="{{URL::to("../public/images/".$post->image)}}" alt="" style="width:60px; height:60px;">
                    @else
                        <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}" alt=""
                             style="width:60px; height:60px;">
                    @endif
                </figure>
                <div class="friend-name">
                    @if ($post->user->is_anonim == 1)
                        <ins><a title="">{{$post->user->anonim_name}}*****</a></ins>
                    @else
                        <ins><a title="">{{$post->user->first_name}} {{$post->user->last_name}}</a></ins>
                    @endif
                    <span>published: {{$post->publish_at}}</span>
                </div>
                <div class="post-meta">

                    <div class="description">
                        <p>
                            {!! nl2br(htmlspecialchars_decode(stripslashes($post->body))) !!}
                        </p>
                    </div>
                    <div class="we-video-info">
                        <ul>
                            <li>
                                <span class="views" data-toggle="tooltip" title="views">
                                    <i class="fa fa-eye"></i>
                                    <ins>{{$post->count_view}}</ins>
                                </span>
                            </li>
                            <li>
                                <span class="comment" data-toggle="tooltip" title="Comments">
                                    <i class="fa fa-comments-o"></i>
                                    <ins>{{ count($post->children()->get()) }}</ins>
                                </span>
                            </li>
                            <li>
                                <span class="like" data-toggle="tooltip" title="like">
                                    @if (!empty($post->likeable) && $post->likeable->type == 1)
                                        <a href="{{ route('status.like.store', ['statuses_id' => $post->id, 'type' => 2]) }}"><i class="fa fa-heart" style="color: red"></i></a>
                                    @else
                                        <a href="{{ route('status.like.store', ['statuses_id' => $post->id, 'type' => 1]) }}"><i class="ti-heart" style="color: red"></i></a>
                                    @endif
                                    <ins style="color: black">{{$post->count_like}}</ins>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="coment-area">
                <ul class="we-comet">
                    @foreach($post->comments as $comment)
                    <li>
                        <div class="comet-avatar">
                            @if (!empty($comment->image))
                                <img src="{{URL::to("../public/images/".$comment->image)}}" alt="" style="width:60px; height:40px;">
                            @else
                                <img src="{{URL::to("../resources/assets/images/resources/img_user_default.jpg")}}" alt="" style="width:60px; height:40px;">
                            @endif
                        </div>
                        <div class="we-comment">
                            <div class="coment-head">
                                @if ($comment->user->is_anonim == 1)
                                    <h5><a href="time-line.html" title="">{{$comment->user->anonim_name}}*****</a></h5>
                                @else
                                    <h5><a href="time-line.html" title="">{{$comment->user->first_name}} {{$comment->user->last_name}}</a></h5>
                                @endif

                                <span>{{$comment->publish_at}}</span>
                            </div>
                            <p style="width: 100% !important;">
                                {!! nl2br(htmlspecialchars_decode(stripslashes($comment->body))) !!}
                            </p>
                        </div>
                    </li>
                    @endforeach
                    @if (!empty($session))
                        <li class="post-comment">
                            <div class="">
                                <form method="post" action="{{ route('status.comment.store') }}">
                                    <input type="hidden" name="parent_id" value="{{$post->id}}">
                                    <textarea name="body" placeholder="Post your comment" style="border-bottom: 1px solid #eeeeee"></textarea>

                                    <button type="submit">Post</button>
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endforeach