<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 32)->unique();
            $table->string('email', 50);
            $table->string('password', 50);
            $table->string('passcode', 50)->nullable();
            $table->string('mobile', 32);
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('image')->nullable();
            $table->date('birth');
            $table->tinyInteger('gender')->comment('1: female, 2: male');
            $table->tinyInteger('is_anonim')->comment('1: true , 2: false');
            $table->tinyInteger('status')->comment('1: active , 2: not active');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'users_migrations_id_fk_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
