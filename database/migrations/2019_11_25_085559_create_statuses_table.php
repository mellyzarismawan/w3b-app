<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations..
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->text('body');
            $table->string('image');
            $table->tinyInteger('status')->comment('1: is publish , 2: not publish');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'statuses_migrations_id_fk_idx');
            $table->index('user_id', 'statuses_migrations_user_id_fk_idx');

            $table->foreign('user_id', 'statuses_migrations_user_id_fk')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
