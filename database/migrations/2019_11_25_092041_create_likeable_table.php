<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikeableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likeable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('statuses_id');
            $table->tinyInteger('type')->comment('1: like , 2: dislike');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'likeable_migrations_id_fk_idx');
            $table->index('user_id', 'likeable_migrations_user_id_fk_idx');
            $table->index('statuses_id', 'likeable_migrations_statuses_id_fk_idx');

            $table->foreign('user_id', 'likeable_migrations_user_id_fk')->references('id')->on('users')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('statuses_id', 'likeable_migrations_statuses_id_fk')->references('id')->on('statuses')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likeable');
    }
}
