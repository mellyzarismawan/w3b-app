<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitableTable extends Migration
{
    /**
     * Run the migrations..
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitable', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('statuses_id');
            $table->integer('count')->default(0);
            $table->tinyInteger('type')->comment('1: see , 2: share');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id', 'visitable_migrations_id_fk_idx');
            $table->index('statuses_id', 'visitable_migrations_statuses_id_fk_idx');

            $table->foreign('statuses_id', 'visitable_migrations_statuses_id_fk')->references('id')->on('statuses')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitable');
    }
}
